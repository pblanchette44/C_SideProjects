#include <stdio.h>
#include <stdlib.h>
void introFunction(int topValue);


int main(int argc,char *argv[]){

	char *ptr;
	int temp = strtol(argv[1],&ptr,10);
	introFunction(temp);

	return 0;
}

void introFunction(int topValue){

	int i;

	int sum = 0;
	for(i = 1;i< topValue;i++){
	
		if(i*3 < topValue)	
			sum += i*3;

		if(i*5 < topValue)
			sum += i*5;
	}

	printf("the sum is %d \n ",sum);
}

