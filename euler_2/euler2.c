#include <stdio.h>

int main(int argc, char *argv[]){

	int fibIndex[] = {1,2};
	int count = 0;
	
	while (fibIndex[1] < 4000000)
	{
		int next = fibIndex[0]+fibIndex[1];
		
		fibIndex[0] = fibIndex[1];
		fibIndex[1] = next;

		if(fibIndex[0] % 2 == 0)
			count+=fibIndex[0];
		if(fibIndex[1] % 2 == 0)
			count+=fibIndex[0];	
	}
	
	printf("the count value is %d \n ",count); 
	return 0;
}
